
# Gets the factorial values of numbers 1 to 10 included e.g. 5! = 5*4*3*2*1

num_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
result_fact = []

# Instead of a list we could use range(1,11)
for i in num_list:
    fact = 1
    for j in range(i - 1):
        fact = fact * (i - j)

    result_fact.append(fact)

print(num_list, "\n", result_fact)
